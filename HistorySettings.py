#!/usr/bin/env python3

import midas.client


if __name__=='__main__':

    group = 'Trigger'
    panel = 'BarsADC10'
    variable = 'Colour'

    client = midas.client.MidasClient("coloring")
    menu = client.odb_get(f"/History/Display/{group}/{panel}/{variable}")
    for i,entry in enumerate(menu):
        print(i,entry,type(entry),menu[i])

    scaler = ['BarsADC11','BarsADC12','BarsADC13','BarsADC14','BarsADC18','BarsADC16']
    for panel in scaler:
        key=f"/History/Display/{group}/{panel}/{variable}"
        for i in range(8):
            print(f"{key}[{i}]",menu[i])
            client.odb_set(f"{key}[{i}]",menu[i])

    client.disconnect()
  
