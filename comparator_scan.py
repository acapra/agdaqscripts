#!/usr/bin/python3

import socket
import sys
import argparse
import subprocess as sp
import midas.client
import numpy as np
import time

def _start_run():
    print("Start Run")
    sp.run(["odbedit", "-c", "start"], stdout=subprocess.PIPE)

def _stop_run():
    print("Stop Run")
    sp.run(["odbedit", "-c", "stop"], stdout=subprocess.PIPE)


def setASDthr(client,thr,end):
    print(f'Settting new threshold {thr} V')

    # set new thr
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_common_setpoint'
    client.odb_set(key,thr)
    
    # make sure that thr changed
    key='/Equipment/BVlv'+end+'/Settings/asd_dac_setpoint'
    #client.odb_set(key,[t]*64)
    
    while client.odb_get(key+'[0]') != thr:
        time.sleep(5)
        
    # Read the value back
    readback = client.odb_get(key)
    print(f"value of {key} is {readback}\t{len(readback)}")
    #------------------------------------------------------


def scanASDthr(client, thr_max, step, end, run):
    print(f'Starting the scan from 0 to {thr_max:1.3f} in {step:1.3f} steps on {end} end')
    for t in np.arange(0.,thr_max+step,step):

        setASDthr(client,t,end)

        if run:
            print("start run")
            _start_run()
            time.sleep(300)
            print("stop run")
            _stop_run()



if __name__=='__main__':    

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())


    parser=argparse.ArgumentParser()
    parser.add_argument("thr", type=float, default=0.3,help="Set maximum value for scan or fixed value when there is no scan (see below)")
    parser.add_argument("end", type=str, default='top',help="Identify the BSC end") # lvdb03
    parser.add_argument("-s","--step",type=float,default=0.01,help="Step size in the scan")
    parser.add_argument("-n","--noscan",action="store_true",help="Do not scan, just set a fixed value")
    parser.add_argument("-r","--run",action="store_true",help="Start a data acquisition for 300s after the threshold has changed")


    
    args=parser.parse_args()

    client = midas.client.MidasClient('ASDcomparatorScan')

    state = client.odb_get("/Runinfo/State")
    if state == midas.STATE_RUNNING:
        print("The experiment is currently running")
        if args.run:
            print("Stop the run first")
            client.disconnect()

    key='/Equipment/BVlv'+args.end+'/Settings/asd_dac_setcommon'
    readback = client.odb_get(key)
    if readback == 0:
        print("Enabling DAC common setpoint")
        client.odb_set(key,1)

    if args.noscan:
        setASDthr(client,args.thr,args.end)
    else:
        scanASDthr(client,args.thr,args.step,args.end,args.run)
    
         
    client.disconnect()
