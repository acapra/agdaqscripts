#!/usr/bin/python3

import argparse
import socket
import sys
from collections import OrderedDict
from midas import client

if __name__ == '__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    print('Running ', sys.argv[0])

    parser=argparse.ArgumentParser(description='MV2 "Three-axis Hall Probe" History Plot',formatter_class=argparse.RawTextHelpFormatter)
    
    parser.add_argument('pwb',help="pwb S/N",nargs='*')
    args=parser.parse_args()

    for imod in args.pwb:
        if 'pwb' in imod or 'PWB' in imod:
            target=imod
        else:
            target=f'pwb{int(imod):02d}'

    client=client.MidasClient("MV2hist")

    pwb_list=client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules")
    idx=pwb_list.index(target)
    NewVariables=[f'CTRL/pwb_mv2_xaxis:pwb_mv2_xaxis[{idx}]', f'CTRL/pwb_mv2_yaxis:pwb_mv2_yaxis[{idx}]', f'CTRL/pwb_mv2_zaxis:pwb_mv2_zaxis[{idx}]']

    hist_plot=OrderedDict()
    hist_plot['Timescale']='10m'
    hist_plot['Zero ylow']=False 
    hist_plot['Show run markers']=True 
    hist_plot['Show values']=True 
    hist_plot['Sort Vars']=False 
    hist_plot['Log axis']=False 
    hist_plot['Minimum']=0
    hist_plot['Maximum']=0 
    hist_plot['Show fill']=False 
    hist_plot['Variables']=NewVariables 
    hist_plot['Label']=['', '', ''] 
    hist_plot['Colour']=['#00AAFF', '#FF9000', '#FF00A0'] 
    hist_plot['Factor']=[0, 0, 0] 
    hist_plot['Offset']=[0, 0, 0] 
    hist_plot['Formula']=['', '', '']

    print(idx,target,'\n',hist_plot)

    client.odb_set(f'/History/Display/Hall/{target}',hist_plot)

    client.disconnect()
