#!/usr/bin/python3

from midas import client
import argparse
import socket
import sys
import subprocess as sp
import requests

def init_pwb(address):
    cmd='init_pwb'

    headers={'Content-Type':'application/json','Accept':'application/json'}
    par={'client_name':'fectrl','cmd':cmd,'args':address}
    payload={'method':'jrpc','params':par,'jsonrpc':'2.0','id': 0}

    url='http://localhost:8080?mjsonrpc'
    print(' request', cmd, address)
    res=requests.post(url, json=payload, headers=headers).json()
    print('response', cmd, address, res['result']['reply'])



def get_pwb_odb():
    return client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules")

def enable_mv2(idx):
    client.odb_set(path=f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_mv2/mv2_enabled[{idx}]",
                    contents=1,create_if_needed=False)
def disable_mv2(idx):
    client.odb_set(path=f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_mv2/mv2_enabled[{idx}]",
                    contents=0,create_if_needed=False)

def set_resolution(idx,r):
    client.odb_set(path=f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_mv2/mv2_resolution[{idx}]",
                    contents=r,create_if_needed=False)

def set_range(idx,s):
    client.odb_set(path=f"/Equipment/CTRL/Settings/PWB/per_pwb_slot_mv2/mv2_range[{idx}]",
                    contents=s,create_if_needed=False)


def enableMV2():
    pwb_list=open('pwb_withMV2.list')
    for ipwb in pwb_list:
        print('Enabling MV2 on',ipwb)
        cmd='esper-tool -v write -d true ' + ipwb.strip() + ' board mv2_enable'
        try:
            sp.check_call(cmd,stderr=sp.STDOUT,shell=True)
        except sp.CalledProcessError as e:
            print('Error raised in ', cmd, e.output)
            return 1
    pwb_list.close()
# esper-tool -v write -d true pwb29 board mv2_enable
        
def setRangeMV2(sens):
    pwb_list=open('pwb_withMV2.list')
    for ipwb in pwb_list:
        print('Enabling MV2 on',ipwb)
        cmd='esper-tool -v write -d '+ str(sens) + ' ' + ipwb.strip() + ' board mv2_range'
        try:
            sp.check_call(cmd,stderr=sp.STDOUT,shell=True)
        except sp.CalledProcessError as e:
            print('Error raised in ', cmd, e.output)
            return 1
    pwb_list.close()
# esper-tool -v write -d 2 pwb29 board mv2_range



if __name__ == '__main__':

    if socket.gethostname() == 'alphagdaq.cern.ch':
        print('Good! We are on', socket.gethostname())
    elif socket.gethostname() == 'daq16.triumf.ca':
        print('Good! We are on', socket.gethostname())
    else:
        sys.exit('Wrong host %s'%socket.gethostname())

    print('Running ', sys.argv[0])


    parser=argparse.ArgumentParser(description='MV2 "Three-axis Hall Probe" Enabler',formatter_class=argparse.RawTextHelpFormatter)
    
    parser.add_argument('pwb',help="pwb S/N",nargs='*')
#    parser.add_argument('pwb',type=int,help="pwb S/N",nargs='?')
#    parser.add_argument('list',type=argparse.FileType('r'),help='pwb S/N list',nargs='?',
#                        default='pwb_withMV2.list')

    parser.add_argument('-s','--range',type=int,default=3,
                            help="Range: 100mT, 300mT, 1T, 3T\ndefault: 3T",choices=range(0,4))
    parser.add_argument('-r','--resolution',type=int,default=2,
                            help="Resolution: 14bits/3kHz, 15bits/1.5kHz, 16bits/0.75kHz, 16bits/0.375kHz\ndefault: 16bits/0.75kHz",choices=range(0,4))
    args=parser.parse_args()

    #enableMV2()
    #setRangeMV2(sens)
    

    client=client.MidasClient("MV2enabler")
    lpwb=get_pwb_odb()

    for imod in args.pwb:
        if 'pwb' in imod or 'PWB' in imod:
            target=imod
        else:
            target=f'pwb{int(imod):02d}'
        idx=lpwb.index(target)
        print('enabling MV2 for',target,'at position',idx)
        enable_mv2(idx)
        set_range(idx,args.range)
        set_resolution(idx,args.resolution)
        init_pwb(target)
    
    client.disconnect()
