#!/usr/bin/python3

import midas.client
from runlog import runlog
from pathlib import Path
from sys import argv

client = midas.client.MidasClient("endrun")
dir=client.odb_get('/Logger/Data dir')

rl = runlog()

#run=903848
#stop=903849
run=int(argv[1])
stop=int(argv[2])

while run < stop:
    jfilename=f'{dir}/run{run:06d}.json'
    jfile=Path(jfilename)
    if jfile.is_file():
        odb = rl.odbFromJson(jfilename)
        rl.appendrun(odb)    
    run+=1

client.disconnect()
