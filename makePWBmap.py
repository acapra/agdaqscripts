#!/usr/bin/env python

import numpy as np
import json
#from datetime import datetime
import argparse


def get_pwb_odb():
    #try:
    from midas import client
    #except OSError:
    #    print('no MIDAS installation found')
    #    print('or no MIDASSYS variable set')
    #    return np.zeros(64)
    try:
        client = client.MidasClient("pwbfinder")
    except OSError:
        print('no MIDAS installation found or no MIDASSYS variable set')
        return np.zeros(64)
    pwb=client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules")
    #print(pwb)
    #pwb=np.reshape(client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules"),(8,8)).T
    print(np.reshape(client.odb_get("/Equipment/CTRL/Settings/PWB/per_pwb_slot/modules"),(8,8)))
    #print(pwb[0,:])
    client.disconnect()
    return pwb


def get_pwb_json(fname):
    data = json.load(fname)
    pwb=data['Equipment']['CTRL']['Settings']['PWB']['per_pwb_slot']['modules']
    #print(pwb)
    return pwb


MAX_PAD_ROW=int(72)
MAX_PAD_SEC=int(4)
#MAX_PWB_RING=int(8)
MAX_PWB_COL=int(8)

def secrow_pwbpos(sec, row):
    sec -= 1
    if sec < 0:
        sec = 31
    pwb_col = int((sec)/MAX_PAD_SEC)
    pwb_row = int((row)/MAX_PAD_ROW)
    #print(pwb_col,pwb_row)
    pwb_pos=[pwb_col,pwb_row]
    return pwb_pos

def secrow_scapos(sec, row):
    scapos=[['A','D'],['B','C']]
    sec -= 1
    if sec < 0:
        sec = 31
    sca_sec=sec%MAX_PAD_SEC
    sca_row=row%MAX_PAD_ROW
    if sca_row<36 and sca_sec<2:
        return scapos[0][0]
    elif sca_row>=36 and sca_sec<2:
        return scapos[1][0]
    if sca_row<36 and sca_sec>=2:
        return scapos[0][1]
    elif sca_row>=36  and sca_sec>=2:
        return scapos[1][1]
    else:
        return 'X'
        
def get_pwb(sec,row):
    pos=secrow_pwbpos(sec, row)
    ipwb=pos[0]*MAX_PWB_COL+pos[1]
    return ipwb

def print_pwb(sec, row):
    pos = secrow_pwbpos(sec, row)
    sca = secrow_scapos(sec, row)
    #print(pos)
    ipwb=get_pwb(sec,row)
    print(' pad sector',sec,'row',row,'\n',
          'in SCA',sca,'\n',
          'corresponds to',pwb_list[ipwb],'\n',
          'at column',pos[0],'ring',pos[1])

def printmap(the_list):
    f=open("pad.map",'w')
    for row in range(576):
        for sec in range(32):
            ipwb=get_pwb(sec,row)
            pwb=the_list[ipwb]
            f.write(f'{sec}\t{row}\t{pwb[3:]}\n')
    f.close()            


def Diff(li1, li2):
    return (list(list(set(li1)-set(li2)) + list(set(li2)-set(li1))))
 

if __name__=='__main__':

    parser = argparse.ArgumentParser(description='Identify PWB and SCA from pad sector and row')
    
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--pad', type=int,
                       nargs=2,
                       help='Pad Sector and Row')
    group.add_argument('--pwb', type=str,
                       nargs=1,
                       help='PWB S/N')

    parser.add_argument('--verify',action='store_true',help='Verify Database Inegrity')

    parser.add_argument('--print',action='store_true',help='Print Map')

    fname='/z12tb/agmini/data/run904214.json'
    parser.add_argument('-o','--odb',
                        nargs='?',
                        type=argparse.FileType('r'),
                        default=fname,
                        help='odb dump')

    args = parser.parse_args()


    if args.verify:
        pwb_list_odb  = get_pwb_odb()
        pwb_list_json = get_pwb_json(args.odb)
        diff_list = Diff( pwb_list_odb, pwb_list_json )
        if len(diff_list) == 0 :
            print(f'current ODB and {args.odb} match')
        else:
            print(diff_list)

    pwb_list = get_pwb_json(args.odb)
    #print(type(args.pad),args.pad)
    print_pwb(args.pad[0],args.pad[1])

    if args.print:
        printmap(pwb_list)
