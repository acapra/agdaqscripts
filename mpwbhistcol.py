#!/usr/bin/python3


varlist=['sfp_rx_power', 'sfp_tx_power','sfp_tx_bias', 'v_sca12', 'v_sca34', 'i_sca12', 'i_sca34', 'temp_board', 'temp_sca_a', 'temp_sca_b','temp_sca_c', 'temp_sca_d']
col=['#0000FF','#00C000','#FF0000','#00C0C0','#FF00FF','#C0C000','#80808','#80FF80']
fcmd=open('mpwbhistcol.com','w')
# call me at end 
# odbedit -c @mpwbhist.com

for c in range(0,8):
    fcmd.write( 'cd /History/Display\n' )
    fcmd.write( 'cd PWBcol%d\n' % c )

    for var in varlist:
        fcmd.write( 'cd %s\n' % var)
        #fcmd.write( 'create string Colour[8][32]\n' )

        for r in range(0,8):
            fcmd.write( 'set Colour[%d] %s\n' % (r,col[r]) )
         
        fcmd.write( 'cd ..\n' )

    fcmd.write( 'cd /\n' )
fcmd.close()
