#!/bin/bash

echo $MIDASSYS
cd ~/online
pwd
echo ""
echo "TOP:"

set -x
odbedit -c "ls /equipment/BVlvtop/Settings/asd_dac_setcommon"
odbedit -c "ls /equipment/BVlvtop/Settings/asd_dac_common_setpoint"
set +x
echo ""
echo "BOT"
set -x
odbedit -c "ls /equipment/BVlvbot/Settings/asd_dac_setcommon"
odbedit -c "ls /equipment/BVlvbot/Settings/asd_dac_common_setpoint"
set +x
