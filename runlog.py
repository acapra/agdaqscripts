#!/usr/bin/python
import sys
import json
import time
import os
import math
import fileinput

"""
This class deals with populating the runlog.txt file with a summary of each run.
"""
class runlog:
  def odb2txt(self, odb, running = False):
    # Start time / Run number / EBuilder events / data logged flag / comments
    run_number = odb['Runinfo']['Run number']
    start_time = odb['Runinfo']['Start time']
    eb_events = odb['Equipment']['EVB']['Statistics']['Events sent']
    written_str =   "Not written   "
    if odb['Logger']['Write data']:
      written_str = "Written       "
    comment = "*NO COMMENT FIELD*"
    if "Edit on start" in odb['Experiment']:
      comment = odb['Experiment']['Edit on start']['Comment']

    if (running):
      eb_events = "Running..."
    else:
      eb_events = str(eb_events) + " events"

    txt = "".join([str(start_time).ljust(27),str(run_number).rjust(5),'  ', eb_events.rjust(20), '  ', written_str, comment])

    return txt

  def odb_params(self, odb, running = False):
    # Start time / Run number / V_cath / V_field / V_anode
    run_number = odb['Runinfo']['Run number']
    start_time = odb['Runinfo']['Start time']
    V_cath = 999
    V_field = 999
    V_anode = 999
    if "CAEN_hvps01" in odb['Equipment']:
      V_cath = odb['Equipment']['CAEN_hvps01']['Variables']['VMON'][0]
      V_field = odb['Equipment']['CAEN_hvps01']['Variables']['VMON'][1]
      V_anode = odb['Equipment']['CAEN_hvps01']['Variables']['VMON'][2]

    txt = "".join([str(start_time).ljust(27),str(run_number).rjust(5),'  ', str(V_cath).rjust(6),'  ', str(V_field).rjust(6),'  ', str(V_field).rjust(6)])

    return txt

  # def handletransition(self, odb, startend):
  #   print "Handling", startend, "transition"
  #   if startend == "start":
  #     self.startrun(odb)
  #   elif startend == "end":
  #     self.endrun(odb)
  #   elif startend == "abort":
  #     self.endrun(odb, True)

  # def startrun(self, odb):
  #   data_dir = odb['Logger']['Data dir']
  #   txt = self.odb2txt(odb, True)
  #   with open (data_dir + "/runlog.log", "a") as myfile:
  #     myfile.write(txt + "\n")

  # def endrun(self, odb, abort = False):
  #   data_dir = odb['Logger']['Data dir']
  #   run_number = odb['Runinfo']['Run number']
  #   eb_events = odb['Equipment']['EVB']['Statistics']['Events sent']

  #   # Replace placeholder with the number of events (the fileinput module
  #   # creates a backup of the current file, and redirects stdout to the
  #   # new version that is opened).
  #   search = ("Running...").ljust(20)
  #   replace = (str(eb_events) + " events").ljust(20)
  #   if abort:
  #     replace = ("Aborted").ljust(20)
  #   found_run = False
  #   replaced = False
  #   print "Replace '" + search + "' with '" + replace + "' for run " + str(run_number)

  #   for line in fileinput.input(data_dir + "/runlog.log", inplace = True):
  #     if str(run_number) in line:
  #       found_run = True
  #       if search in line:
  #         line = line.replace(search, replace)
  #         replaced = True
  #     sys.stdout.write(line)

  #   if not found_run:
  #     # Somehow we missed the start of this run - just add the details now
  #     self.appendrun(odb)

  #   if found_run and not replaced:
  #     # It seems we called end-of-run twice. That's odd.
  #     pass

  def appendrun(self, odb):
    data_dir = odb['Logger']['Data dir']
    txt = self.odb2txt(odb)
    with open (data_dir + "/runlog.log", "a") as myfile:
      myfile.write(txt + "\n")
    print(txt)
    params = self.odb_params(odb)
    with open (data_dir + "/odb_params.log", "a") as myfile:
      myfile.write(params + "\n")

  def odbFromJson(self, filename):
    with open(filename, "r") as file:
      data = file.read()
      #odb = json.loads(data, "ISO-8859-1")
      odb = json.loads(data)
      return odb

  def bulkupload(self):
    seen_datadirs = []
    dir = "."
    os.chdir(dir)
    for filename in sorted(os.listdir(dir)):
      if (filename.find("json") == -1):
        continue

      odb = self.odbFromJson(filename)
      data_dir = odb['Logger']['Data dir']
      if (data_dir not in seen_datadirs):
        # First time we've seen this data dir - wipe the current file
        seen_datadirs.append(data_dir)
        with open (data_dir + "/runlog.log", "w") as myfile:
          myfile.write("# Start time / Run number / EBuilder events / data logged flag / comments\n")
        with open (data_dir + "/odb_params.log", "w") as myfile:
          myfile.write("# Start time / Run number / V_cath / V_field / V_anode\n")

      self.appendrun(odb)

  def dummy(self):
    print("Dummy function")

if __name__ == "__main__":
  runlog().bulkupload()
