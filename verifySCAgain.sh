#!/bin/bash

pwb="pwb12"
if [[ $1 != "" ]]; then
    pwb=$1
fi

for x in `seq 0 3`; do
    vvv=$(esper-tool read ${pwb} sca${x} version)
    ggg=$(esper-tool read ${pwb} sca${x} gain)
    echo "${pwb} sca${x} version: ${vvv} gain: ${ggg}"
done
